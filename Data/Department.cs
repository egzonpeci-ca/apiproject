﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class Department : BaseEntity
    {
        public Guid Id { get; set; }
        public string DepartmentName { get; set; }
        public bool IsActive { get; set; }
    }
}

