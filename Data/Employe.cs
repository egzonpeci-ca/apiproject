﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data
{
     public class Employe : BaseEntity
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public DateTime DateBirth { get; set; }
        public string FullAddress { get; set; }
        public DateTime HireDate { get; set; }
        public string Postion { get; set; }
        public decimal Salary { get; set; }

        [Column("DepartmentId")]
        [ForeignKey("Department")]
        public Guid? DepartmentId { get; set; }
        public virtual Department Department { get; set; }
    }
}
