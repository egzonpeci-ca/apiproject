﻿using Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.DepartmentSer
{
    public interface IDepartmentService
    {
        List<Department> GetAll();
        void Update(Department department);
        void Insert(Department department);
        Department GetById(Guid id);
        void Delete(Department department);

    }
}
