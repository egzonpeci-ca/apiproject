﻿using Data;
using Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.DepartmentSer
{
    public class DepartmentService : IDepartmentService
    {
        private IRepository<Department> _repo;
        public DepartmentService(IRepository<Department> repo)
        {
            _repo = repo;
        }

        public void Delete(Department department)
        {
            _repo.Delete(department);
        }

        public List<Department> GetAll()
        {
            return _repo.GetAll();
        }

        public Department GetById(Guid id)
        {
            return _repo.GetById(id.ToString());
        }

        public void Insert(Department department)
        {
            _repo.Insert(department);
        }

        public void Update(Department department)
        {
            _repo.Update(department);
        }
    }
}
