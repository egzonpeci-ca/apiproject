﻿using Data;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Users
{
    public interface IUsersService
    {
        AppUser GetById(string Id);
        IEnumerable<AppUser> GetAll();

        AuthenticateResponse Authenticate(AppUser model);


    }
}

