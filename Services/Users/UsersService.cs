﻿using Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Repository;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Web.Helpers;

namespace Services.Users
{
    public class UsersService : IUsersService
    {
        private readonly IUserRepository<AppUser> appUsersRepository;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<AppUser> userManager;
        private readonly IOptions<AppSettings> _appSettings;

        public UsersService(IUserRepository<AppUser> appUsersRepository, RoleManager<IdentityRole> roleManager, UserManager<AppUser> userManager, IOptions<AppSettings> appSettings)
        {
            this.appUsersRepository = appUsersRepository;
            this.roleManager = roleManager;
            this.userManager = userManager;
            _appSettings = appSettings;
        }

        public AuthenticateResponse Authenticate(AppUser model)
        {
            //model.PasswordHash = Convert.ToBase64String(
            //                              System.Security.Cryptography.SHA256.Create()
            //                                    .ComputeHash(Encoding.UTF8.GetBytes(model.PasswordHash))
            //                             );
            model.PasswordHash = "AQAAAAEAACcQAAAAEEhx0J2x/WpoPIA6epA5Vr/NnHYe0hLZzQN4jVMpueH1w4792x3vSDwvAnB89GW+6g==";
            var user = this.appUsersRepository.GetSingle(x => x.UserName == model.UserName && x.PasswordHash == model.PasswordHash);
            if (user == null) return null;
            var token = generateJwtToken(user);
            return new AuthenticateResponse(user, token);
        }

        public IEnumerable<AppUser> GetAll()
        {
            return this.appUsersRepository.Get().ToList();
        }

        public AppUser GetById(string Id)
        {
            return this.appUsersRepository.Get(filter: x => x.Id == Id).FirstOrDefault();
        }

        // helper methods

        private string generateJwtToken(AppUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.UTF8.GetBytes(_appSettings.Value.TokenKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddMinutes(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        //string GenerateTokenString(string username, DateTime expires, Claim[] claims = null)
        //{
        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var key = Encoding.ASCII.GetBytes(_appSettings.TokenKey);
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(
        //         claims ?? new Claim[]
        //        {
        //            new Claim(ClaimTypes.Name, username)
        //        }),
        //        //NotBefore = expires,
        //        Expires = expires.AddMinutes(2),
        //        SigningCredentials = new SigningCredentials(
        //            new SymmetricSecurityKey(key),
        //            SecurityAlgorithms.HmacSha256Signature)
        //    };

        //    return tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
        //}




    }
}
