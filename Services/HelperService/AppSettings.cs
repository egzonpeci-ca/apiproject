namespace Web.Helpers
{
    public class AppSettings
    {
        public AppSettings()
        {
            TokenKey = "eyJpZCI6IjliZTJlNGZkLThhNzEtNGEyYS05N2YzLTNmMzE3NWVkYjlmZiIsIm5iZiI6MTY";
        }

        public string TokenKey { get; set; }
    }
}