﻿using Data;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Services.EmployesSer
{
    public class EmployesService : IEmployesService
    {
        private IRepository<Employe> _repo;

        public EmployesService(IRepository<Employe> repo)
        {
            _repo = repo;
        }

        public void Delete(Employe employe)
        {
            _repo.Delete(employe);

        }

        public List<Employe> GetAll()
        {
            IEnumerable<string> includes = new string[] { "Department" };
            return _repo.Get(includeProperties: includes).ToList();
        }

        public List<Employe> GetByDepartmentId(Guid? departmentid)
        {
            return _repo.GetAll();

        }

        public Employe GetById(Guid id)
        {
            return _repo.GetById(id.ToString());
        }

        public Employe GetByIdIncludeDepartment(Guid id)
        {
            IEnumerable<string> includes = new string[] { "Department" };
            return _repo.Get(includeProperties: includes, filter: x => x.Id == id).FirstOrDefault();
        }

        public void Insert(Employe employe)
        {
            _repo.Insert(employe);

        }

        public void Update(Employe employe)
        {
            _repo.Update(employe);
        }
    }
}
