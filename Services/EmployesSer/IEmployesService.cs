﻿using Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.EmployesSer
{
    public interface IEmployesService
    {
        List<Employe> GetAll();
        Employe GetById(Guid id);
        Employe GetByIdIncludeDepartment(Guid id);
        List<Employe> GetByDepartmentId(Guid? departmentid);
        void Update(Employe employe);
        void Insert(Employe employe);
        void Delete(Employe employe);


    }
}
