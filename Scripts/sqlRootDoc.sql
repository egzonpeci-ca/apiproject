USE lawdata3


DECLARE @RootId  NVARCHAR(250)= '976e44ea-ebeb-47cf-bc42-08d93bc73ac1'
 --  roots with translations
SELECT r.*,rt.Title, rt.Description
	FROM Roots r
	INNER JOIN RootTranslations rt ON rt.RootId = r.Id
	WHERE r.id = @RootId

--  roots with relates documents
SELECT  rrt.id,rrt.RootId,rrt.LanguageId,rrt.Title, rrt.Description,r.*
	FROM Roots r 
	inner join   RelatedLegalDocs rld ON r.Id = rld.LegalDocId
	inner join Roots rl ON rl.Id  = rld.RelatedLegalDocId  
	INNER JOIN RootTranslations rrt ON rrt.RootId = rl.Id 
	WHERE r.id =@RootId
 
--  notes with and translated orderd by orderid
SELECT lt.Name,n.*, nt.Notes
	FROM Nodes n 
	INNER JOIN  NodeTranslations nt ON nt.NodeId = n.Id
	INNER JOIN Lists l on n.NodeTypeId = l.Id and l.ListTypeId = 1 
	      INNER JOIN ListTranslations lt on lt.ListId = l.Id and lt.[LanguageId]=1
	WHERE N.RootId = @ROOTID
 
 -- select all related nodes base on roots
 SELECT *,ln.Id as lnid  ,ln.IsDeleted as lndeledet
 FROM Nodes n 
	inner join LinkedNodes ln on n.Id= ln.NodeId 
	inner join Nodes rn on rn.Id = ln.LinkedNodeId
	      inner join NodeTranslations lnt on lnt.NodeId = rn.Id --and LanguageId =2
where n.RootId = @RootId

 -- select all related nodes base on roots
 SELECT *,lnt.Content content, rn.Number, rn.Notes
 FROM Nodes n 
	inner join LinkedNodes ln on n.Id= ln.LinkedNodeId 
	inner join Nodes rn on rn.Id = ln.NodeId
	      inner join NodeTranslations lnt on lnt.NodeId = rn.Id --and LanguageId =2
where n.RootId = @RootId

 