﻿GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (1, N'Deçan', N'Dečane', N'Decan', N'Decan', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (2, N'Gjakovë', N'Djakovica', N'Gjakova', N'Gjakova', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (3, N'Gllogoc', N'Glogovac', N'Glogovac', N'Glogovac', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (4, N'Gjilan', N'Gnjilane', N'Gjilan', N'Gjilan', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (5, N'Dragash', N'Dragaš', N'Dragash', N'Dragash', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (6, N'Istog', N'Istok', N'Istog', N'Istog', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (7, N'Kaçanik', N'Kačanik', N'Kacanik', N'Kacanik', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (8, N'Klinë', N'Klina', N'Klina', N'Klina', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (9, N'Fushë Kosovë', N'Kosovo Polje', N'Fushe Kosova', N'Fushe Kosova', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (10, N'Kamenicë', N'Kamenica', N'Kamenica', N'Kamenica', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (11, N'Mitrovicë', N'Mitrovica', N'Mitrovica', N'Mitrovica', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (12, N'Leposaviq', N'Leposavič', N'Leposaviq', N'Leposaviq', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (13, N'Lipjan', N'Lipjan', N'Lipjan', N'Lipjan', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (14, N'Novobërdë', N'Novo Brdo', N'Novo Brdo', N'Novo Brdo', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (15, N'Obiliq', N'Obilič', N'Obilic', N'Obilic', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (16, N'Rahovec', N'Orahovac', N'Rahovec', N'Rahovec', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (17, N'Pejë', N'Peč', N'Peja', N'Peja', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (18, N'Podujevë', N'Podujevo', N'Podujevo', N'Podujevo', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (19, N'Prishtinë', N'Priština', N'Pristina', N'Pristina', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (20, N'Prizren', N'Prizren', N'Prizren', N'Prizren', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (21, N'Skënderaj', N'Srbica', N'Skenderaj', N'Skenderaj', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (22, N'Shtime', N'Štimlje', N'Stimlje', N'Stimlje', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (23, N'Shtërpcë', N'Štrpce', N'Shtrpce', N'Shtrpce', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (24, N'Suharekë', N'Suva Reka', N'Suva Reka', N'Suva Reka', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (25, N'Ferizaj', N'Uroševac', N'Ferizaj', N'Ferizaj', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (26, N'Viti', N'Vitina', N'Viti', N'Viti', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (27, N'Vushtrri', N'Vučitrn', N'Vushtrri', N'Vushtrri', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (28, N'Zubin Potok', N'Zubin Potok', N'Zubin Potok', N'Zubin Potok', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (29, N'Zveçan', N'Zvečan', N'Zvecan', N'Zvecan', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (30, N'Malishevë', N'Mališevo', N'Malisheve', N'Malisheve', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (31, N'Hani i Elezit', N'Elez Han', N'Hani i Elezit', N'Hani i Elezit', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (32, N'Mamushë', N'Mamuša', N'Mamusha', N'Mamusha', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (33, N'Junik', N'Junik', N'Junik', N'Junik', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (34, N'Kllokot', N'Klokot', N'Klokot', N'Klokot', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (35, N'Graçanicë', N'Gračanica', N'Gracanice', N'Gracanice', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (36, N'Ranillug', N'Ranillug', N'Ranillug', N'Ranillug', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (37, N'Partesh', N'Parteš', N'Partesh', N'Partesh', 0, 0)
GO
INSERT [dbo].[Municipalities] ([id], [nameAl], [nameSr], [nameEn], [nameTr], protocolCounter, receiptCounter) VALUES (38, N'Mitrovica e Veriut', N'Severna Mitrovica ', N'North Mitrovica', N'North Mitrovica', 0, 0)
GO
SET IDENTITY_INSERT [dbo].[Municipalities] OFF
GO
