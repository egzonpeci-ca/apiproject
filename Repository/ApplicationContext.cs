﻿using Microsoft.EntityFrameworkCore;
using Data;
using System.Linq;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.Linq.Expressions;

namespace Repository
{

    public class CustomRole : IdentityRole<string>
    {
        public CustomRole() { }
        public CustomRole(string name) { Name = name; }
    }

    public class CustomUserRole : IdentityUserRole<string> { }
    public class CustomUserClaim : IdentityUserClaim<string> { }
    public class CustomUserLogin : IdentityUserLogin<string> { }


    public class ApplicationContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {

        }

        public DbSet<Department> Departments { get; set; }
        public DbSet<Employe> Employes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyGlobalFilters<bool>("IsDeleted", false);
            seedData4UserAndRoles(modelBuilder);
        }

        #region Seeds
        private static void seedData4UserAndRoles(ModelBuilder modelBuilder)
        {
            string Roleadminid, adminid;
            Roleadminid = Guid.NewGuid().ToString();

            modelBuilder.Entity<IdentityRole>().HasData(
                  new IdentityRole { Name = "Administrator", Id = Roleadminid }
                );


            adminid = Guid.NewGuid().ToString();
            var user = new AppUser { Id = adminid, FirstName = "Admin", LastName = "ADMIN", Email = "admin@admin.com", NormalizedEmail = "admin@admin.COM", UserName = "admin", NormalizedUserName = "admin", PhoneNumber = "+111111111111", EmailConfirmed = true, PhoneNumberConfirmed = true, SecurityStamp = Guid.NewGuid().ToString("D"), active = true };
            user.PasswordHash = (new PasswordHasher<AppUser>()).HashPassword(user, "123456");
            modelBuilder.Entity<AppUser>().HasData(user);
            modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string> { UserId = adminid, RoleId = Roleadminid });

        }

        #endregion

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        //public override async Task SaveCHangesAsync()
        //{
        //    AddTimestamps();
        //    return await base.SaveChangesAsync();
        //}

        private void AddTimestamps()
        {
            var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));

            // var currentUsername = !string.IsNullOrEmpty(System.Web.HttpContext.Current?.User?.Identity?.Name) ? HttpContext.Current.User.Identity.Name : "Anonymous";

            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((BaseEntity)entity.Entity).CreatedDate = DateTime.Now;
                    ((BaseEntity)entity.Entity).LastChangedDate = DateTime.Now;
                    ((BaseEntity)entity.Entity).IsDeleted = false;
                }

                ((BaseEntity)entity.Entity).CreatedDate = DateTime.Now;
                ((BaseEntity)entity.Entity).LastChangedDate = DateTime.Now;
                // ((BaseEntity)entity.Entity).IsDeleted = false;
            }
        }

        //public override int SaveChanges()
        //{
        //    var userId = "";
        //    this.ChangeTracker.DetectChanges();
        //    var added = this.ChangeTracker.Entries()
        //                .Where(t => t.State == EntityState.Added)
        //                .Select(t => t.Entity)
        //                .ToArray();

        //    foreach (var entity in added)
        //    {
        //        if (entity is BaseEntity)
        //        {
        //            var track = entity as BaseEntity; // BS: this should be an Interface type
        //            track.CreatedDate = DateTime.Now;
        //            track.LastChangedDate = DateTime.Now;
        //        }
        //    }

        //    var modified = this.ChangeTracker.Entries()
        //                .Where(t => t.State == EntityState.Modified)
        //                .Select(t => t.Entity)
        //                .ToArray();

        //    foreach (var entity in modified)
        //    {
        //        if (entity is BaseEntity)
        //        {
        //            var track = entity as BaseEntity; // BS: this should be an Interface type
        //            track.CreatedDate = DateTime.Now;
        //            track.LastChangedDate = DateTime.Now;
        //        }
        //    }

        //    return base.SaveChanges();
        //}
    }
    public static class ExtendionMethods
    {
        public static void ApplyGlobalFilters<T>(this ModelBuilder modelBilder, string propertyName, T value)
        {
            foreach (var entityType in modelBilder.Model.GetEntityTypes())
            {
                var foundProperty = entityType.FindProperty(propertyName);

                if (foundProperty != null && foundProperty.ClrType == typeof(T))
                {
                    var newParam = Expression.Parameter(entityType.ClrType);

                    var filter = Expression.
                        Lambda(Expression.Equal(Expression.Property(newParam, propertyName),
                        Expression.Constant(value)), newParam);

                    modelBilder.Entity(entityType.ClrType).HasQueryFilter(filter);
                }

            }
        }
    }
}
