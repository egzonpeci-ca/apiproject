﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Repository.Migrations
{
    public partial class add_newfiled_isActiveDepartments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "619b6f5b-3604-4c76-b610-34706a3e1fe2", "c50de5e1-d0b7-4c69-8502-decb63d302b1" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "c50de5e1-d0b7-4c69-8502-decb63d302b1");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "619b6f5b-3604-4c76-b610-34706a3e1fe2");

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Departments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "f33b56ba-ce08-47ef-8066-84c4dc04bce6", "3d62a3dd-9fe9-40c7-aec7-fbd2cd2b7c66", "Administrator", null });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "FirstName", "LastName", "active" },
                values: new object[] { "9be2e4fd-8a71-4a2a-97f3-3f3175edb9ff", 0, "c1ccd46d-c472-434d-b553-977587582a33", "AppUser", "admin@admin.com", true, false, null, "admin@admin.COM", "admin", "AQAAAAEAACcQAAAAEEhx0J2x/WpoPIA6epA5Vr/NnHYe0hLZzQN4jVMpueH1w4792x3vSDwvAnB89GW+6g==", "+111111111111", true, "6b868e5c-47e4-4162-9306-e6a0018e1340", false, "admin", "Admin", "ADMIN", true });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "9be2e4fd-8a71-4a2a-97f3-3f3175edb9ff", "f33b56ba-ce08-47ef-8066-84c4dc04bce6" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "UserId", "RoleId" },
                keyValues: new object[] { "9be2e4fd-8a71-4a2a-97f3-3f3175edb9ff", "f33b56ba-ce08-47ef-8066-84c4dc04bce6" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "f33b56ba-ce08-47ef-8066-84c4dc04bce6");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "9be2e4fd-8a71-4a2a-97f3-3f3175edb9ff");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Departments");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "c50de5e1-d0b7-4c69-8502-decb63d302b1", "7e22284c-4a59-429d-b943-6f2e0d14506e", "Administrator", null });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName", "FirstName", "LastName", "active" },
                values: new object[] { "619b6f5b-3604-4c76-b610-34706a3e1fe2", 0, "2abf7ed5-8d15-4538-a387-3c2690d26c0f", "AppUser", "admin@admin.com", true, false, null, "admin@admin.COM", "admin", "AQAAAAEAACcQAAAAEIqX+QvSzDveI5mgPSZBO5nuzNCvVMa3KFlLOZYMfNtb2mRl5vAkV5s97CuIoFjOsA==", "+111111111111", true, "33a5f0e9-87bb-4dbc-835f-364c58d9abdc", false, "admin", "Admin", "ADMIN", true });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[] { "619b6f5b-3604-4c76-b610-34706a3e1fe2", "c50de5e1-d0b7-4c69-8502-decb63d302b1" });
        }
    }
}
