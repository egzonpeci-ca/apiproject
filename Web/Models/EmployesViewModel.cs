﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class EmployesViewModel : StaticTokenViewModel
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public DateTime DateBirth { get; set; }
        public string FullAddress { get; set; }
        public DateTime HireDate { get; set; }
        public string Postion { get; set; }
        public decimal Salary { get; set; }
        public string Department { get; set; }
    }
    public class CreateEmployesViewModel : StaticTokenViewModel
    {
        public string FullName { get; set; }
        public DateTime DateBirth { get; set; }
        public string FullAddress { get; set; }
        public DateTime HireDate { get; set; }
        public string Postion { get; set; }
        public decimal Salary { get; set; }
        public Guid? DepartmentId { get; set; }
    }

    public class UpdateEmployesViewModel : StaticTokenViewModel
    {
        public Guid Id { get; set; }

        public string FullName { get; set; }
        public DateTime DateBirth { get; set; }
        public string FullAddress { get; set; }
        public DateTime HireDate { get; set; }
        public string Postion { get; set; }
        public decimal Salary { get; set; }
        public Guid? DepartmentId { get; set; }
    }


}
