﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class DepartmentViewModel : StaticTokenViewModel
    {
        public  Guid Id { get; set; }
        public string DepartmentName { get; set; }
        public bool IsActive { get; set; }
    } 
    
    public class CreateDepartmentViewModel : StaticTokenViewModel
    {
        public string DepartmentName { get; set; }
        public bool IsActive { get; set; }

    }

    public class UpdateDepartmentViewModel : StaticTokenViewModel
    {
        [Required]
        public Guid Id { get; set; }
        public string DepartmentName { get; set; }
        public bool IsActive { get; set; }

    }
}
