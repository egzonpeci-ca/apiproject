﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Web.Models
{
    public class AppUserDTO
    {
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
    }
}
