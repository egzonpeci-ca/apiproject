﻿using Microsoft.Extensions.DependencyInjection;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Services.Users;
using Data;
using Microsoft.AspNetCore.Identity;
using Services.DepartmentSer;
using Services.EmployesSer;

namespace Web.DI
{
    public static class DependencyConfig
    {
        public static void AddDependencies(this IServiceCollection services)
        {
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<IDepartmentService, DepartmentService>();
            services.AddScoped<IEmployesService, EmployesService>();

        }
    }
}
