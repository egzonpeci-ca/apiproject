﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Web.Helpers;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        AppSettings appSettings = new AppSettings();
        public readonly string TokenIsNotValid = "Not authorized";
        public IActionResult Index()
        {
            return View();
        }

        public bool IsValidToken(string StaticToken)
        {
            if (StaticToken == appSettings.TokenKey)
            {
                return true;
            }
            return false;
        }
    }
}
