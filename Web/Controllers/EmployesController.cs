﻿using AutoMapper;
using Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.EmployesSer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Controllers
{
    [Route("api/employes")]
    [ApiController]
    public class EmployesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IEmployesService _employesService;
        BaseController baseController = new BaseController();

        public EmployesController(IMapper mapper, IEmployesService employesService)
        {
            _mapper = mapper;
            _employesService = employesService;
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] string token)
        {
            if (!baseController.IsValidToken(token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }

            var result = _mapper.Map<List<EmployesViewModel>>(_employesService.GetAll());
            return Ok(result);

        }

        [HttpGet]
        [Route("GetById/{Id}/{token}")]
        public IActionResult GetById(Guid Id, string token)
        {

            if (!baseController.IsValidToken(token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }
            var result = _mapper.Map<EmployesViewModel>(_employesService.GetByIdIncludeDepartment(Id));
            if (result == null)
            {
                return BadRequest("This worker does not exist, or has been deleted");
            }
            return Ok(result);
        }


        [HttpPost]
        [Route("create")]
        public IActionResult CreateDepartment([FromBody] CreateEmployesViewModel model)
        {

            if (!baseController.IsValidToken(model.token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }

            try
            {
                var mappedDepartment = _mapper.Map<Employe>(model);
                _employesService.Insert(mappedDepartment);
                return Ok(true);
            }
            catch (Exception error)
            {
                return BadRequest(error);
            }
        }

        [HttpPut]
        [Route("update")]
        public IActionResult UpdateDepartment([FromQuery] UpdateEmployesViewModel model)
        {

            if (!baseController.IsValidToken(model.token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }
            try
            {
                var dbResult = _employesService.GetById(model.Id);
                if (dbResult == null)
                {
                    return BadRequest("This worker does not exist, or has been deleted");
                }
                var mapped = _mapper.Map(model, dbResult);
                _employesService.Update(mapped);
                return Ok(true);
            }
            catch (Exception error)
            {
                return BadRequest(error);
            }
        }


        [HttpDelete]
        [Route("delete/{employerid}/{token}")]
        public IActionResult DeleteDepartment(Guid employerid, string token)
        {
            if (!baseController.IsValidToken(token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }
            try
            {
                var mappedDB = _mapper.Map<Employe>(_employesService.GetById(employerid));
                if (mappedDB == null)
                {
                    return BadRequest("This worker does not exist, or has been deleted");
                }
                _employesService.Delete(mappedDB);
                return Ok(true);
            }
            catch (Exception error)
            {
                return BadRequest(error);

            }
        }
    }
}
