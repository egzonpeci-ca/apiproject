﻿using AutoMapper;
using Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services.DepartmentSer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Web.Models;

namespace Web.Controllers
{
    [Route("api/departments")]
    [ApiController]
    public class DepartmentsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IDepartmentService _departmentService;
        BaseController baseController = new BaseController();


        public DepartmentsController(IMapper mapper, IDepartmentService departmentService)
        {
            _mapper = mapper;
            _departmentService = departmentService;
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] string token)
        {
            if (!baseController.IsValidToken(token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }

            var result = _mapper.Map<List<DepartmentViewModel>>(_departmentService.GetAll());
                return Ok(result);
         
        }

        [HttpGet]
        [Route("GetById/{Id}/{token}")]
        public IActionResult GetById(Guid Id, string token)
        {

            if (!baseController.IsValidToken(token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }
            var result = _mapper.Map<DepartmentViewModel>(_departmentService.GetById(Id));
            if(result==null)
            {
                return BadRequest("This department does not exist or has been deleted");
            }
            return Ok(result);
        }


        [HttpPost]
        [Route("create")]
        public IActionResult CreateDepartment([FromBody] CreateDepartmentViewModel model)
        {

            if (!baseController.IsValidToken(model.token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }

            try
            {
                var mappedDepartment = _mapper.Map<Department>(model);
                _departmentService.Insert(mappedDepartment);
                return Ok(true);
            }
            catch (Exception error)
            {
                return BadRequest(error);
            }
        }

        [HttpPut]
        [Route("update")]
        public IActionResult UpdateDepartment([FromQuery] UpdateDepartmentViewModel model)
        {

            if (!baseController.IsValidToken(model.token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }
            try
            {
                var dbResult = _departmentService.GetById(model.Id);
                if (dbResult == null)
                {
                    return BadRequest("This department does not exist");
                }
                var mapped = _mapper.Map(model, dbResult);
                _departmentService.Update(mapped);
                return Ok(true);
            }
            catch (Exception error)
            {
                return BadRequest(error);
            }
        }


        [HttpDelete]
        [Route("delete/{departmentid}/{token}")]
        public IActionResult DeleteDepartment(Guid departmentid, string token)
        {
            if (!baseController.IsValidToken(token))
            {
                return BadRequest(baseController.TokenIsNotValid);
            }
            try
            {
                var mappedDB = _mapper.Map<Department>(_departmentService.GetById(departmentid));
                if (mappedDB == null)
                {
                    return BadRequest("This department does not exist");
                }
                _departmentService.Delete(mappedDB);
                return Ok(true);
            }
            catch (Exception error)
            {
                return BadRequest(error);

            }
        }
    }
}
