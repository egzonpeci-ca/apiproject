﻿using AutoMapper;
using Data;
using Microsoft.AspNetCore.Identity;
using Web.Models;

namespace Web.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AppUserDTO, AppUser>();

            //Department

            CreateMap<Department, DepartmentViewModel>();
            CreateMap<CreateDepartmentViewModel, Department>();
            CreateMap<UpdateDepartmentViewModel, Department>(); 
          

            //Employes
            
            CreateMap<Employe, EmployesViewModel>().ForMember(d => d.Department, opt => opt.MapFrom(c => c.Department.DepartmentName));
            CreateMap<CreateEmployesViewModel, Employe>();
            CreateMap<UpdateEmployesViewModel, Employe>();



        }
    }
}
